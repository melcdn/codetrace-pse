# CoDeTRAce PSE
Nothing really fancy here. This is just a sample project for GitLab CI/CD Demo.

## About the script
It is a simple webscrapping script that extracts the details of the provided stock symbol from a PSE website ([Pesobility](https://www.pesobility.com/)).
The solution utilizes below Python libraries:
1. [requests](https://pypi.org/project/requests/) - used to fetch data from the target website.
2. [beautifulsoup4](https://pypi.org/project/beautifulsoup4/) - used to parse fetched html data.
## Requirements
These applications should be installed on your computer.
1. [Python 3.x](https://www.python.org/downloads/)

## Repository
1. [PyPi](https://pypi.org/project/codetrace-pse)
2. [GitLab](https://gitlab.com/melcdn/codetrace-pse)

## Installation via pip
#### Downloading the package
1. Open your `windows terminal`.
2. Go to your working directory (e.g. Desktop) then install using the `pip` command.
```
cd %USERPROFILE%\Desktop\
pip install codetrace-pse
```
#### Testing the installation
Executing below command should output the **help** document.
```
pse-cli -h

usage: pse-cli [-h] -s SYMBOL

CoDeTRAce PSE-CLI v1.0.0

optional arguments:
  -h, --help            show this help message and exit
  -s SYMBOL, --symbol SYMBOL
                        Symbol of the stock you want to check. (e.g. BPI, JFC)
```
#### Sample run with arguments
```
pse-cli -s JFC

================================================
CoDeTRAce PSE-CLI v1.0.0
================================================
Name            : Jollibee Foods Corporation
Symbol          : JFC
Current Price   : P193.00 (-1.03%)
Previous Price  : P195.00
Open            : P195.00
Volume          : 813,110
52-Week High    : P228.00
52-Week Low     : P91.10
Shares          : 1,107,698,270
================================================
Source : https://www.pesobility.com/stock/JFC
================================================
```
